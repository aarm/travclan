import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import CompanyUserList from "./container/customer-list/customer-list";
import Bid from "./container/customer-list/bid";

const Routes = ({}) => {
	return (
			<Switch>
				<Route exact path="/">
					{/* eslint-disable-next-line react/jsx-no-undef */}
					<CompanyUserList/>
				</Route>
				<Route exact path="/bid">
					{/* eslint-disable-next-line react/jsx-no-undef */}
					<Bid/>
				</Route>
			</Switch>
	);
};

export default Routes;

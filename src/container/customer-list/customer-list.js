import React,{ useEffect, useState, useContext }   from 'react'

import {withRouter} from 'react-router';
import axios from "axios";
import ReactPaginate from "react-paginate";


function  CompanyUserList({ history }) {
    // eslint-disable-next-line no-undef
    let [emailList, setEmailList] = useState([]);
    let [page, setpage] = useState([]);

    const [currentPage, setCurrentPage] = useState(0);
    function getCustomer() {
        let url = 'https://intense-tor-76305.herokuapp.com/merchants'
        axios.get(url).then(response => {
            setEmailList(response.data)
        })
    }

    useEffect(() => {
        getCustomer()

    }, []);

    function handlePageClick({ selected: selectedPage }) {
        setCurrentPage(selectedPage);
    }

    function maximumBid(data) {
        let bid = []
        for (let i =0 ; i< data.bids.length; i++) {
            bid.push(data.bids[i].amount)
        }
        return Math.max(...bid)
    }

    function bidSaveInLocal(e, data) {
        const song = e.target.getAttribute('data-item');
        console.log('We need to get the details for ', data);
        localStorage.setItem('bid', JSON.stringify(data))
        history.push("/bid");
    }

    const PER_PAGE = 10;
    const offset = currentPage * PER_PAGE;
    const currentPageData = emailList
        .slice(offset, offset + PER_PAGE);
    const pageCount = Math.ceil(emailList.length / PER_PAGE);

        return (
            <div>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Bid</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        currentPageData.map((e, i) => {
                            return (
                                <tr role='button' key={i} data-item={e} onClick={event => bidSaveInLocal(event, e)}>
                                    <td>{i+1}</td>
                                    <td>{e.id}</td>
                                    <td>{e.firstname}</td>
                                    <td>{e.lastname}</td>
                                    <td>{e.email}</td>
                                    <td>{e.phone}</td>
                                    <td>{maximumBid(e)}</td>
                                </tr>
                            );
                        })
                    }

                    </tbody>
                </table>
                <div className="App">
                    <ReactPaginate
                        previousLabel={"← Previous"}
                        nextLabel={"Next →"}
                        pageCount={pageCount}
                        onPageChange={handlePageClick}
                        containerClassName={"pagination"}
                        previousLinkClassName={"pagination__link"}
                        nextLinkClassName={"pagination__link"}
                        disabledClassName={"pagination__link--disabled"}
                        activeClassName={"pagination__link--active"}
                    />
                </div>
            </div>
        )
}

export default withRouter(CompanyUserList)

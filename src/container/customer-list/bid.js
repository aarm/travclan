import React, {useEffect, useState, useContext} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {withRouter} from 'react-router';


function Bid(props) {

    let [bidList, setBidList] = useState([]);
    let [img, setimg] = useState([]);

    useEffect(() => {
        const bid = localStorage.getItem('bid')
        // console.log(JSON.parse(bid))
        setBidList(JSON.parse(bid))
        setimg(bidList.avatarUrl)
    }, []);


    return (
        <>
            <div className='container mt-5 card p-4'>
                <h4>Bid Details</h4>
                <div className='row'>
                        {
                            bidList?.bids && bidList?.bids.map((value, index) => {
                               return            <div className='col-md-4 col-sm-12'>
                                <div className='card mt-3' >
                                    <div className='card-body'>
                                        <h5 className='text-capitalize'>{value.carTitle}</h5>
                                        <div className='row p-3 pl-0'>
                                            <div className='col-4 p-0'><b>Amount - </b>{value.amount}</div>
                                            <div className='col-6'><b>Created - </b>{value.created}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            })
                        }
                </div>
            </div>
        </>
    )
}

export default withRouter(Bid)

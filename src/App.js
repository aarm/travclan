import './App.css';
import Routes from './Routes';
import {
    BrowserRouter as Router,
} from "react-router-dom";
function App() {
  return (
    <div className="App">
      <Router>
            <div className="content-wrapper">
              <div className="content">
                  <Routes />
              </div>
            </div>
      </Router>
    </div>
  );
}

export default App;
